/*
 Gaetan Simonin 
 id étudiant : 11801730
 */

#include "trees.h"

// construction of a tree by pointer
void cons_tree(struct node ** ptr_tree, int val, struct node *left, struct node *right)
{
	if (*ptr_tree == NULL){
		*ptr_tree = malloc(sizeof(struct node));
		(*ptr_tree)->val = val;
		(*ptr_tree)->left = left;
		(*ptr_tree)->right = right;
	}
}

// initialize un empty tree
void mk_empty_tree(struct node ** ptr_tree)
{
	*ptr_tree = NULL;
}

// is tree empty?
bool is_empty(struct node *tree)
{
	return tree==NULL;
}

// is tree a leaf?
bool is_leaf(struct node *tree)
{
	return(!is_empty(tree) && is_empty(tree->left) && is_empty(tree->right));
}

// add x in a bst wtr its value.
void add(struct node **ptr_tree, int x)
{
	if ((*ptr_tree) == NULL) cons_tree(ptr_tree, x, NULL, NULL);
    else if ((*ptr_tree)->val > x) add(&((*ptr_tree)->left), x);
    else add(&((*ptr_tree)->right), x);
}

// print values of tree in ascendant order
void print_tree(struct node *tree)
{
	if (!is_empty(tree)){
    	print_tree(tree->right);
    	printf("%d ", tree->val);
    	print_tree(tree->left);
    }
}

// build a tree "add"ing values of the file fp
void load_tree(FILE *fp, struct node **ptr_tree)
{
    int nb = 0;
    while(fscanf(fp, "%d", &nb) != EOF) add(ptr_tree, nb);
}

// Free all memory used by the tree
void free_tree(struct node **ptr_tree)
{
	if ((*ptr_tree) != NULL) {
        free_tree(&(*ptr_tree)->right);
        free_tree(&(*ptr_tree)->left);
        free(*ptr_tree);
        *ptr_tree = NULL;
	}
}

int count_nodes(struct node *tree){
	if (tree != NULL){
		return(1 + count_nodes(tree->left) + count_nodes(tree->right));
	}
	return 0;
}

void get_max(struct node *tree, int *max, int firstLoop){
	if (tree != NULL){
		if (firstLoop == 1) *max = tree->val - 1;
		if (tree->val > *max) *max = tree->val;
		get_max(tree->left, max, 0);
		get_max(tree->right, max, 0);
	}
}

void get_min(struct node *tree, int *min, int firstLoop){
	if (tree != NULL){
		if (firstLoop == 1) *min = ((tree->val) + 1);
		if (tree->val < *min) *min = tree->val;
		get_max(tree->left, min, 0);
		get_max(tree->right, min, 0);
	}
}

void med_tree(struct node *tree, int medRef, int *indice, int *med){
	if(tree != NULL){
		if (*indice == medRef) *med = tree->val;
		(*indice)++;
		med_tree(tree->left, medRef, indice, med);
		med_tree(tree->right, medRef, indice, med);
	}
}

void print_sup_nodes(struct node *tree, int seuil){
	if (!is_empty(tree)){
    	print_sup_nodes(tree->right, seuil);
    	if (tree->val >= seuil)	printf("%d ", tree->val);
    	print_sup_nodes(tree->left, seuil);
    }
}

struct node *del_node(struct node *node, int val){
    if(node != NULL){

    		// si val est inférieure à la valeur du noeud, on recherche dans le sous-arbre gauche
    	if (val < node->val) node->left = del_node(node->left, val);

        	// sinon si val est supérieure à la valeur du noeud, on recherche dans le sous-arbre droite
		else if (val > node->val) node->right = del_node(node->right, val);

		else {
				// Cas d'un fils unique à droite
			if (node->left == NULL){
				struct node *temp = node->right;
            	free(node); node = NULL;
            	return temp;
			}

				// Cas d'un fils unique à gauche
			else if (node->right == NULL){
				struct node *temp = node->left;
            	free(node); node = NULL;
            	return temp;
			}

				// Si node a deux enfants (à gauche et à droite)
    		struct node *successeur = node->right;
    		while(successeur->left != NULL) successeur = successeur->left;
 			struct node *predecesseur = node->left;
    		while(predecesseur->right != NULL) predecesseur = predecesseur->right;

    		node->val = successeur->val;

    			// on supprime le successeur et le predecesseur
    		node->right = del_node(node->right, successeur->val);
    		node->left = del_node(node->left, successeur->val);

    		return node;
		}
  	}
  	return NULL;
}
