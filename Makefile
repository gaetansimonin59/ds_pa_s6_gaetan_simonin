#
#	Gaetan Simonin 
#	id etudiant 11801730
#

###################### VARIABLES #################################

# Compilateur
	cc = gcc
# Executable
	exec = main

# Librairie
	lib = trees

# Compilateur de librairie
	lib-cc = ar r

# Dependances
	dep = $(lib).c $(lib).h $(exec).c

# Drapeau Warning
	wFlag = no
# Options de compilation
	options =

# Drapeau Gdb
	gdbFlag = no

###################### COMMANDS #################################

default: commands				# Lancer le programme simplement (Ecrire : make)

gdb: options = -g				# Lancer le programme en mode debug avec gdb (Ecrire : make gdb)
gdb: launcher = gdb
gdb: gdbFlag = yes
gdb: commands

all: options = -W -Wall -Wextra -pedantic		# Lancer le programme avec tous les warnings possibles (Ecrire : make all).
all: wFlag = yes								
all: all-warning-compil					

clean: clean						# Supprimer tout les fichiers utilisés et plus utile (Ecrire : make clean)

mrpropper: clean-all



##################### WORKING COMMANDS DETAILED #################################

commands: $(dep)
ifeq ($(gdbFlag), yes)
	@echo "\nGénération en mode debug" # fonctionne pas je ne sais pas pourquoi 
else 
	@echo "\nGénération en mode release"
endif

		# compilation
	$(cc) -c *.c $(options) 	

		# création de la librairie
	@echo "\n\n"
	$(lib-cc) $(lib).a $(lib).o 		
	$(lib-cc)cs $(lib).a $(lib).o $(exec).o

		# compilation de l'executable
	@echo "\n\n"
	$(cc) -o $(exec) $(exec).o $(lib).a $(options)

	@echo "\nLancer l'executable avec la commande ./main cheminVersLeFichierATraiter\n"

all-warning-compil: $(dep)
	@echo "\nGénération en mode release"

		# compilation
	$(cc) -c *.c $(options)
		
		# création de la librairie
	@echo "\n\n"
	$(lib-cc) $(lib).a $(lib).o 
	$(lib-cc)cs $(lib).a $(lib).o $(exec).o
	
	@echo "\n\n"
	clang -c *.c -Weverything

		# compilation de l'executable
	@echo "\n\n"
	$(cc) -o $(exec) $(exec).o $(lib).a $(options)
	@echo "\nLancer l'executable avec la commande ./main cheminVersLeFichierATraiter\n"


###################### CLEANING COMMANDS #################################

clean:
	rm -fr *.o

clean-all:
	rm -f *.o
	rm -fr $(exec)