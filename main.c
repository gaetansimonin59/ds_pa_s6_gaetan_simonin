/*
 	Gaetan Simonin
 	id etudiant 11801730
 */

#include <stdio.h>

#include "trees.h"

int main (int argc, char *argv[])
{
	if (argc != 2){
		printf("Nombre d'arguments incorrect, respecter la forme : ./executable cheminVersLeFichierATraiter\n");
		return EXIT_FAILURE;
	}

	FILE *fp = fopen(argv[1], "r");

	if (fp == NULL){
		printf("Impossible d'ouvrir le fichier\n");
		return EXIT_FAILURE;
	}

	struct node *tree; // Création de l'arbre
	mk_empty_tree(&tree); // Initialisation de l'arbre

	load_tree(fp, &tree); // Remplissage de l'arbre avec les données du fichier fp

	print_tree(tree); // Affichage de l'arbre

	int nbNodes = count_nodes(tree);
	printf("\nNombre de noeuds : %d\n", nbNodes); // Compte le nombre de noeuds de l'arbre
	
	int max = 0, min = 0;
	get_max(tree, &max, 1); // Determine la valeur maximale de l'arbre
	printf("\nValeur maximale dans l'arbre : %d\n", max);
	get_min(tree, &min, 1); // Determine la valeur minimale de l'arbre Mais ne fonctionne pas ...
	printf("\nValeur minimale dans l'arbre : %d\n", min);

	int medRef = nbNodes/2; // Renvoie la medianne de l'arbre (un entier) car on divise par un entier
	int indice = -1, medTree = 0; // On met indice à -1 pour combler le decallage du zero (tableau)
	med_tree(tree, medRef, &indice, &medTree);
	printf("\nValeur medianne de l'arbre : %d\n", medTree);

	int seuil = 0;
	printf("Entrer un seuil : \n");
	scanf("%d", &seuil);
	print_sup_nodes(tree, seuil);
	printf("\n");

	int aSupprimer = 0;
	printf("Entrer la valeur du noeud à supprimer : \n");
	scanf("%d", &aSupprimer);

	printf("Etat de l'arbre avant suppression : \n");
	print_tree(tree);
	printf("\n\n Etat de l'arbre apres suppression : \n");
	del_node(tree, aSupprimer); // Ne fonctionne pas ...
	print_tree(tree);
	printf("\n");

	free_tree(&tree); // Desallocation de l'arbre
	fclose(fp); // Fermeture du fichier fp

	return EXIT_SUCCESS;
}

